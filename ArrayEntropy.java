/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

import java.util.*;

/**
 *
 * @author alex
 */
public class ArrayEntropy {
    
    private final ArrayList<Double> S;
    
    public ArrayEntropy(){
        S=new ArrayList<>();
    }
    
    public void addEntropy(double S){
        this.S.add(S);
    }
    
    public Double getEntropy(int i){
        return S.get(i);
    }
    
    public Double getMin(){
        ArrayList<Double> arr;
        
        arr=(ArrayList<Double>)this.S.clone();
        
        Collections.sort(arr);
        return arr.get(0);
    }
    
    public Double getMax(){
        ArrayList<Double> arr;
        
        arr=(ArrayList<Double>)this.S.clone();
        
        Collections.sort(arr);
        return arr.get(arr.size()-1);
    }
    
    public int size(){
        return S.size();
    }

    
    
}
