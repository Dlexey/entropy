/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

/**
 *
 * @author alex
 */
public class Triangle {
    
    private MyPoint p1;
    private MyPoint p2;
    private MyPoint p3;
    
    private double S;
    
    public Triangle(){
        this.p1= new MyPoint();
        this.p2=new MyPoint();
        this.p3=new MyPoint();    
        
        S=0;       
    }
        
    public Triangle(MyPoint p1, MyPoint p2, MyPoint p3 ){
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;  
        
        isTriangle();    
        
        S=Area();       
    }
    
    public void set(MyPoint p1, MyPoint p2, MyPoint p3){
        this.p1=p1;
        this.p2=p2;
        this.p3=p3;    
        
        isTriangle();    
        
        S=Area();          
    }
    
    private double Area(){     
        //http://e-maxx.ru/algo/oriented_area            
        
        return Math.abs((p2.getX() - p1.getX()) * (p3.getY() - p1.getY()) - (p2.getY() - p1.getY()) * (p3.getX() - p1.getX()))/2;     
    }
    
    public double getArea(){        
        return S;                      
    }

    /**
     * @return the p1
     */
    public MyPoint getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(MyPoint p1) {
        this.p1 = p1;
        isTriangle();  
        S=Area();  
    }

    /**
     * @return the p2
     */
    public MyPoint getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(MyPoint p2) {
        this.p2 = p2;
        isTriangle();  
        S=Area();  
    }

    /**
     * @return the p3
     */
    public MyPoint getP3() {
        return p3;
    }

    /**
     * @param p3 the p3 to set
     */
    public void setP3(MyPoint p3) {
        this.p3 = p3;
        isTriangle();  
        S=Area();  
    } 
    
    private void isTriangle(){
        if ((p1.equals(p2))||(p1.equals(p3))){
            
            System.out.println(p1.getX()+" "+p1.getY());
            System.out.println(p2.getX()+" "+p2.getY());
            System.out.println(p3.getX()+" "+p3.getY());
            System.out.println();
            
            System.out.println("Не удаётся создать треугольник. Равные вершины.");
            System.exit(0);
        }  
    }
    
}
