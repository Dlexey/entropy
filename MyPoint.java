/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

import java.awt.geom.*;
import java.util.*;

/**
 *
 * @author alex
 */
public class MyPoint extends Point2D implements Cloneable{
    
    private double x;
    private double y;
	
	public MyPoint() {
		x=0;
		y=0;
	}

	public MyPoint(double x, double y) {
		this.x=x;
		this.y=y;
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public void setLocation(double x, double y) {
		this.x=x;
		this.y=y;		
	}
        
        public static Comparator<MyPoint> YComparator = new Comparator<MyPoint>() {	
            @Override
            public int compare(MyPoint p1, MyPoint p2) {
                return (int) (p1.getY() - p2.getY());
            }        
        };
        
        @Override
        public boolean equals(Object obj) {
            if(!(obj instanceof MyPoint))
                return false;
            MyPoint ref = (MyPoint)obj;
            return ((this.x==ref.x)&&(this.y==ref.y));
        }

    @Override
    public int hashCode() {
        
        int hash = 3;
        hash = 71 * hash + (int) (java.lang.Double.doubleToLongBits(this.x) ^ (java.lang.Double.doubleToLongBits(this.x) >>> 32));
        hash = 71 * hash + (int) (java.lang.Double.doubleToLongBits(this.y) ^ (java.lang.Double.doubleToLongBits(this.y) >>> 32));
        return hash;
    }
    
    @Override
    public MyPoint clone(){
        
        MyPoint clone;
        
        clone=(MyPoint) super.clone();        
        return clone;        
    }

        
    
}
