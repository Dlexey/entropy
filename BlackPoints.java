/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;


import java.awt.*;
import java.awt.image.*;
import java.util.*;


/**
 *
 * @author alex
 */
public class BlackPoints implements Cloneable, Arrayable {
    
    private ArrayList<MyPoint> black_points_arr;
    private MyPoint point=null;
    
            
    public BlackPoints(BufferedImage image_in){
        this.black_points_arr = new ArrayList<>();
        //читаем все чёрные точки
        for(int i=0;i<image_in.getWidth();i++){
            for(int j=0;j<image_in.getHeight();j++){
                if (image_in.getRGB(i, j)==Color.black.getRGB()){
                    point=new MyPoint(i,j);
                    black_points_arr.add(point);
                }
                
            }
        }
        
    }
    
    public MyPoint getNorthPoint(){
        ArrayList<MyPoint> point_here;
	point_here=(ArrayList<MyPoint>) this.black_points_arr.clone();
        
        if (point_here.isEmpty()){
            System.out.println("В файле типа in.png отсутсвуют чёрные пиксели");
            System.exit(0);
        }
				
	Collections.sort(point_here,MyPoint.YComparator);
        	
	return point_here.get(0);       
    }
    
    @Override
    public BlackPoints clone() throws CloneNotSupportedException{
        
        BlackPoints clone=null;
        
        try {
            clone=(BlackPoints) super.clone();
	      
	} catch (CloneNotSupportedException e) {
	    throw new InternalError(e.getMessage());
	}
        
        clone.black_points_arr=(ArrayList<MyPoint>) this.black_points_arr.clone();
        
        return clone;        
    }   
    
    @Override
    public ArrayList<MyPoint> getArray(){        
        return black_points_arr;    
    }
    
}
