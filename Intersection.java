/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

/**
 *
 * @author alex
 */
public final class Intersection {
    
    
    
    public static boolean segments(MyPoint p1, MyPoint p2, MyPoint p3, MyPoint p4){
        //http://e-maxx.ru/algo/segments_intersection_checking
        
        //пересечине двух отрезков ab и cd
        
        MyPoint a=p1;
        MyPoint b=p2;
        
        MyPoint c=p3;
        MyPoint d=p4;
        
        boolean flag_cd=shadowIntersect (a.getX(), b.getX(), c.getX(), d.getX())
		&& shadowIntersect (a.getY(), b.getY(), c.getY(), d.getY())
		&& Area(a,b,c) * Area(a,b,d) <= (double)0
		&& Area(c,d,a) * Area(c,d,b) <= (double)0;
        
        return flag_cd;               
    }
    
    private static double Area(MyPoint p1, MyPoint p2, MyPoint p3){     
        //http://e-maxx.ru/algo/oriented_area      
        
        return (p2.getX() - p1.getX()) * (p3.getY() - p1.getY()) - (p2.getY() - p1.getY()) * (p3.getX() - p1.getX());    
    }

    private static boolean shadowIntersect(double a, double b, double c, double d){
        //http://e-maxx.ru/algo/segments_intersection_checking
        
        double stuff;
        
        if (a>b){
            stuff=a;
            a=b;
            b=stuff;
        }    
        
        if (c>d){
            stuff=c;
            c=d;
            d=stuff;
        } 
        
        return Math.max(a,c) <= Math.min(b,d);
        
    }  
    
}
