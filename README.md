# Entropy

Entropy - это программа, которая для заданной области определяет зоны наиболее и наименее равномерно отстоящие от границ области.
Очевидно, что центр прямоугольника - это точка наиболее равномерно отстоящая от границ.
А угол прямоугольника - точка наименее равномерно отстоящая. Для фигур сложной формы такие точки не очевидны.
Например для эллипса, как оказалось, наименее равномерно удалённые от границ точки располагаются на границе эллипса несколько выше вершин большой полуоси.
Такой подход может оказаться полезным при организации пространств помещений.
Очевидно, что в точках наиболее равномерно отстоящих от границ удобнее всего располагать информационные панели, стойки секретарей, места для ожидания и т.д.
А наименее равномерно отстоящие - это укромные местечки.

Entropy is a program that, for a given area, defines the zones that are the most and least uniformly spaced from the boundaries of the region.
Obviously, the center of the rectangle is the point most uniformly spaced from the boundaries.
And the corner of the rectangle is the point that is least uniformly spaced. For figures of complex shape, such points are not obvious.
For example, for an ellipse, as it turned out, the points that are least uniformly removed from the boundaries are located on the boundary of the ellipse somewhat higher than the vertices of the large semiaxis.
This approach can be useful in the organization of spaces of premises. Obviously, at the points most evenly spaced from the borders, it is most convenient to have information panels, secretarial stands, waiting areas, etc.
And the least uniformly spaced apart are secluded places.