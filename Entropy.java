/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

import java.awt.image.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;
import javax.imageio.*;
import java.awt.Color;


/**
 *
 * @author alex
 */
public class Entropy {
    
    private static ArrayList<MyPoint> insideAreaArr;
    private static ArrayList<MyPoint> boundArr;
    private static ArrayList<Triangle> triangls;
    private static ArrayEntropy ArrayS;
    
    private static double minS;
    private static double maxS;  

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File folder = new File(".");
	String[] files = folder.list();
		
	//------------------------------------- начинаем искать входной файл -------------------------------------------------------------
	int count=0;
        for (String file : files) {
            if (file.startsWith("in.")) {
                count++;
            }
        }
		
	if (count==0) {
		System.err.println("Ошибка: В папке должен присутствовать входной файл типа in.png или любого другого формата");
		System.exit(0);
	}
		
	if (count>1) {
		System.err.println("Ошибка: Входной файл типа in.png должен быть в единственном экземпляре");
		System.exit(0);
	}
		
	File file_in=null;
	if (count==1) {			
                for (String file : files) {
                    if (file.startsWith("in.")) {
                        file_in = new File(file);
                    }
                }			
	}
        //------------------------------------- нашли входной файл -------------------------------------------------------------
        BufferedImage image_in=null;
        try {//прочитали входной файл	
            image_in = ImageIO.read(file_in);
        } catch (IOException ex) {
            Logger.getLogger(Entropy.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //вытащили из входного файла все чёрные точки
        BlackPoints allBlackPoints=new BlackPoints(image_in);
        //нашли самую северную чёрную точку
        MyPoint northPoint=allBlackPoints.getNorthPoint();        
        //нашли границу       
        MyBound bounds=new MyBound(northPoint,image_in);
        //нашли первый пиксель внутренней области
        int x=(int)northPoint.getX();
        int y=(int)northPoint.getY();
        while (image_in.getRGB(x, y)==Color.BLACK.getRGB()){
            y++;
        }
        MyPoint northInsidePoint=new MyPoint(x,y);
        //нашли внутреннюю область
        MyArea insideArea=new MyArea(northInsidePoint,image_in);
        //нашли внутренние границы
        ArrayList<MyPoint> insideBounds;
        insideBounds = Aline.subtract(allBlackPoints,bounds);   
        
        ArrayList<MyPoint> contourOut= Aline.contour(insideArea, image_in);//нашли внутренний контур
        contourOut=Aline.contourTOcontour(new MyPoint(x,y-1), contourOut,bounds, image_in);//нашли внешний контур
        
        insideAreaArr=insideArea.getArray();
        boundArr=contourOut;
        triangls=new ArrayList<>();
        ArrayS = new ArrayEntropy();     
                
        entropyExe();
                
        //=========================================================================== вывод ========================================
                
        BufferedImage image_out=new BufferedImage(image_in.getWidth(),image_in.getHeight(),BufferedImage.TYPE_INT_RGB);
        
        BufferedImage image_out_insideArea=new BufferedImage(image_in.getWidth(),image_in.getHeight(),BufferedImage.TYPE_INT_RGB);
        
        BufferedImage image_out_insideBounds=new BufferedImage(image_in.getWidth(),image_in.getHeight(),BufferedImage.TYPE_INT_RGB);
        
        BufferedImage image_out_result=new BufferedImage(image_in.getWidth(),image_in.getHeight(),BufferedImage.TYPE_INT_RGB);
               
        
        File file_out = new File("./out_bounds.png");
        File file_out_insideArea = new File("./out_insideArea.png");
        File file_out_insideBounds = new File("./out_insideBounds.png");
        File file_out_result = new File("./out_result.png");         
        RGB entropyRGB=new RGB();
        Color c;
                
        for (int i=0;i<boundArr.size();i++){            
                image_out.setRGB((int)boundArr.get(i).getX(), (int)boundArr.get(i).getY(), Color.white.getRGB());                          
        }
        
        for (int i=0;i<insideAreaArr.size();i++){                           
                image_out_insideArea.setRGB((int)insideAreaArr.get(i).getX(), (int)insideAreaArr.get(i).getY(), Color.white.getRGB()); 
        }
        
        for (int i=0;i<insideBounds.size();i++){                           
                image_out_insideBounds.setRGB((int)insideBounds.get(i).getX(), (int)insideBounds.get(i).getY(), Color.white.getRGB()); 
        }
        
        for (int i=0;i<ArrayS.size();i++){            
            c=entropyRGB.getColor(maxS, minS, ArrayS.getEntropy(i));
            image_out_result.setRGB((int)insideAreaArr.get(i).getX(), (int)insideAreaArr.get(i).getY(), c.getRGB()); 
        }       
        
        
        try {	
            ImageIO.write(image_out, "png", file_out);
            ImageIO.write(image_out_insideArea, "png", file_out_insideArea);
            ImageIO.write(image_out_insideBounds, "png", file_out_insideBounds);       
            ImageIO.write(image_out_result, "png", file_out_result);  
        } catch (IOException ex) {
            Logger.getLogger(Entropy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
    private static void entropyExe(){
//        int ii=0;
//        int jj=0;
//        int mm=0;
//                
//        
//        for (int i=0;i<insideAreaArr.size();i++){
//            if ((insideAreaArr.get(i).getX()==426)&&(insideAreaArr.get(i).getY()==397)){
//                ii=i;
//                System.out.println(insideAreaArr.get(i).getX()+" "+insideAreaArr.get(i).getY()+" "+i);
//            }
//        }
//        
//        for (int i=0;i<boundArr.size();i++){
//            if ((boundArr.get(i).getX()==317)&&(boundArr.get(i).getY()==283)){
//                jj=i;
//                System.out.println(boundArr.get(i).getX()+" "+boundArr.get(i).getY()+" "+i);
//                //System.exit(0);
//            }
//            if ((boundArr.get(i).getX()==390)&&(boundArr.get(i).getY()==360)){
//                mm=i;
//                System.out.println(boundArr.get(i).getX()+" "+boundArr.get(i).getY()+" "+i);
//                //System.exit(0);
//            }            
//        }
        //-----------------------------------------------------------------------------------------------------------
        Triangle tria;      
       
        boolean flag;
                
        for (int i=0;i<insideAreaArr.size();i++){
            for (int j=0; j<boundArr.size();j++){
//                i=ii;
//                j=jj;
                int k=j+1;
                if (j==boundArr.size()-1){
                    k=0;
                }                
                
                flag=true;
                tria=new Triangle(insideAreaArr.get(i),boundArr.get(j),boundArr.get(k));
                if (tria.getArea()!=(double)0){
                    for (int m=0; m<boundArr.size();m++){
                        int n=m+1;
                        if (m==boundArr.size()-1){
                            n=0;
                        }
//                        m=mm;
                        if (j!=m&&j!=n&&j!=m-1&&j!=n-1&&j!=n+1&&j!=m+1){
                            flag=!Intersection.segments(insideAreaArr.get(i),boundArr.get(j),boundArr.get(m),boundArr.get(n));   
                            if (!flag){                                
                                break;//for m
                            }
                        }                                              
                    }
                    if (flag){
                        triangls.add(tria);  
                    }                    
                }                
                              
            }    
            
            double summaArea=0;
            for (Triangle triangl : triangls) {
                summaArea+=triangl.getArea();
            }

            double S=0;
            double p;
            for (Triangle triangl : triangls) {
                p=triangl.getArea()/summaArea;    
                S-=p*Math.log(p);
            }         
            
            ArrayS.addEntropy(S);
            triangls.clear();    
            
            System.out.println(i+": "+insideAreaArr.size());
        }        
        
        minS=ArrayS.getMin();
        maxS=ArrayS.getMax();        
    }
}