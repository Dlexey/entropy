/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;
import java.awt.Color;

/**
 *
 * @author alex
 */
public class RGB {
    int r;
    int g;
    int b;
    
    public RGB(){
        r=0;
        g=0;
        b=0;
    }
    
    public Color getColor(double max, double min, double S){        
        
        double alpha;
                
        if (S<(min+(max-min)/3)){//от синего до зелёного
            alpha=Math.toRadians((S-min)/((max-min)/3)*90);
            r=0;
            g=(int)(255*Math.sin(alpha));  
            b=(int)(255*Math.cos(alpha));
                     
        }
        if ((S>=(min+(max-min)/3))&(S<(min+(max-min)*2/3))){//от зелёного до желтого
            r=(int)(255*(S-min-(max-min)/3)/(max-min)*3);
            g=255;
            b=0;
        }
        if ((S>=(min+(max-min)*2/3))&(S<=(min+(max-min)))){//от желтого до красного
            r=255;
            g=(int)(255*(1-(S-(min+(max-min)*2/3))/((max-min)/3)));
            b=0;
        }
        
        Color c=new Color(r, g, b);
				
	return c;		
        
    }    
}
