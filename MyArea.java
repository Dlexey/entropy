/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

import java.awt.image.*;
import java.util.*;

/**
 *
 * @author alex
 */
public class MyArea implements Arrayable {
    private final Collection<MyPoint> bound = new LinkedHashSet<>();      
    private final BufferedImage image_in;
    private ArrayList<MyPoint> points;
    private final int pixel_color;  
    
    
    
    public MyArea(MyPoint zeroPoint, BufferedImage image_in){
        this.image_in=image_in;        
        this.pixel_color=image_in.getRGB((int)zeroPoint.getX(), (int)zeroPoint.getY());
        
        int size;
        
        size=bound.size();
        bound.add(zeroPoint);
        
        int count=1;
        while (size<bound.size()){
            size=bound.size(); 
                                    
            points=getPoints(bound,count);
            boolean flag;            
            for (int i=0;i<count;i++){
                for (int x=(int)(points.get(i).getX()-1);x<=points.get(i).getX()+1;x++){
                    for (int y=(int)(points.get(i).getY()-1);y<=points.get(i).getY()+1;y++){     
                        flag=insideArea(i,x,y);                        
                        
                        if ((image_in.getRGB(x,y)==pixel_color)&flag){
                            bound.add(new MyPoint(x,y));                            
                        }            
                    }                
                }
            }
            count=bound.size()-size;              
        }        
    }
    
    private ArrayList<MyPoint> getPoints(Collection<MyPoint> bound, int count){
        Iterator<MyPoint> iterator;
        ArrayList<MyPoint> p=new ArrayList<>();        
        
        iterator=bound.iterator();   
        int i=0;
        while (iterator.hasNext()){                 
            i++;
            if (bound.size()-i>=count){
                iterator.next();
            }else{
                p.add(iterator.next());
            }            
        }        
        return p;  
    }  
    
    protected boolean insideArea(int i, int x, int y){
        boolean flag=true;
        if ((image_in.getRGB((int)points.get(i).getX(),(int)points.get(i).getY()-1)!=pixel_color)&(image_in.getRGB((int)points.get(i).getX()+1,(int)points.get(i).getY())!=pixel_color)){
            if (image_in.getRGB((int)points.get(i).getX()+1,(int)points.get(i).getY()-1)==pixel_color){
                if ((x==(int)points.get(i).getX()+1)&(y==(int)points.get(i).getY()-1)){
                    flag=false;
                }
            }                            
        }
        if ((image_in.getRGB((int)points.get(i).getX()+1,(int)points.get(i).getY())!=pixel_color)&(image_in.getRGB((int)points.get(i).getX(),(int)points.get(i).getY()+1)!=pixel_color)){
            if (image_in.getRGB((int)points.get(i).getX()+1,(int)points.get(i).getY()+1)==pixel_color){
                if ((x==(int)points.get(i).getX()+1)&(y==(int)points.get(i).getY()+1)){
                    flag=false;
                }
            } 
        }
        if ((image_in.getRGB((int)points.get(i).getX()-1,(int)points.get(i).getY())!=pixel_color)&(image_in.getRGB((int)points.get(i).getX(),(int)points.get(i).getY()+1)!=pixel_color)){
            if (image_in.getRGB((int)points.get(i).getX()-1,(int)points.get(i).getY()+1)==pixel_color){
                if ((x==(int)points.get(i).getX()-1)&(y==(int)points.get(i).getY()+1)){
                    flag=false;
                }
            } 
        }
        if ((image_in.getRGB((int)points.get(i).getX(),(int)points.get(i).getY()-1)!=pixel_color)&(image_in.getRGB((int)points.get(i).getX()-1,(int)points.get(i).getY())!=pixel_color)){
            if (image_in.getRGB((int)points.get(i).getX()-1,(int)points.get(i).getY()-1)==pixel_color){
                if ((x==(int)points.get(i).getX()-1)&(y==(int)points.get(i).getY()-1)){
                    flag=false;
                }
            } 
        }
        return flag;
    }
    
    @Override
    public ArrayList<MyPoint> getArray(){        
        return new ArrayList<>(bound);    
    }    
}
