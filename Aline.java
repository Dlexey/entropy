/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entropy;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;

/**
 *
 * @author alex
 */
public final class Aline {
    
    public static ArrayList<MyPoint> subtract(Arrayable arr1, Arrayable arr2){
        //вычитает из arr1 элементы arr2
        ArrayList<MyPoint> array1=arr1.getArray();
        ArrayList<MyPoint> array2=arr2.getArray();       
       
        return subtractF(array1,array2);        
    }
    
    public static ArrayList<MyPoint> subtract(ArrayList<MyPoint> array1, ArrayList<MyPoint> array2){
        //вычитает из arr1 элементы arr2
               
        return subtractF(array1,array2);         
    }
    
    public static ArrayList<MyPoint> subtract(ArrayList<MyPoint> array1, Arrayable arr2){
        //вычитает из arr1 элементы arr2
        ArrayList<MyPoint> array2=arr2.getArray();           
                
        return subtractF(array1,array2);           
    }
    
    public static ArrayList<MyPoint> subtract(Arrayable arr1, ArrayList<MyPoint> array2){
        //вычитает из arr1 элементы arr2
        ArrayList<MyPoint> array1=arr1.getArray();       
                
        return subtractF(array1,array2);          
    }   
    
    private static ArrayList<MyPoint> subtractF(ArrayList<MyPoint> array1, ArrayList<MyPoint> array2){
        //вычитает из arr1 элементы arr2
        
        for (int j=0;j<array2.size();j++){
            for(int i=0;i<array1.size();i++){
                if (array1.get(i).equals(array2.get(j))){
                    array1.remove(i);                                  
                }
            }            
        }
        return array1;    
        
    }
    
    //--------------------------------------------- contour --------------------------------------------------
    
    public static ArrayList<MyPoint> contour(Arrayable arr1, BufferedImage image_in){
        //выделяет внешнюю границу у определённой области точек
        ArrayList<MyPoint> array1=arr1.getArray();   
        int pixel_color; 
        int size;
        boolean flag;
        
        size=0;
        while (size!=array1.size()){
            size=array1.size();
            for (int i=0;i<array1.size();i++){
                flag=false;
                pixel_color=image_in.getRGB((int)array1.get(i).getX(), (int)array1.get(i).getY());
                for (int x=(int)(array1.get(i).getX()-1);x<=array1.get(i).getX()+1;x++){
                    for (int y=(int)(array1.get(i).getY()-1);y<=array1.get(i).getY()+1;y++){
                        if ((image_in.getRGB(x,y)!=pixel_color)){                            
                            flag=true;
                            break;
                        }                    
                    }  
                    if (flag) break;
                }
                if (!flag) array1.remove(i);
                flag=false;
            }
              
        }
    return array1;  
    }
    
    //------------------------------------------------ contour sort -------------------------------------------------------
    
    public static ArrayList<MyPoint> contourSort(Arrayable array1, BufferedImage image_in){
         //сорирует массив точек контура, так чтобы точки шли друг за другом 
        
        ArrayList<MyPoint> contour=array1.getArray();               
                
        return contourSortF(contour, image_in);  
    }
    
    public static ArrayList<MyPoint> contourSort(ArrayList<MyPoint> array1, BufferedImage image_in){
        //сорирует массив точек контура, так чтобы точки шли друг за другом        
                
        return contourSortF(array1, image_in);  
    }
    
    
    public static ArrayList<MyPoint> contourSortF(ArrayList<MyPoint> array1, BufferedImage image_in){
        //сорирует массив точек контура, так чтобы точки шли друг за другом 
        
        ArrayList<Dist> found= new ArrayList<>();        
        ArrayList<MyPoint> bound = new ArrayList<>();
        ArrayList<MyPoint> contour=array1;  
        MyPoint currentPoint=contour.get(0);
        Dist distance;        
        int pixel_color;         
               
        bound.add(currentPoint);
        while (true){                
            pixel_color=image_in.getRGB((int)currentPoint.getX(), (int)currentPoint.getY());
            
            for (int x=(int)(currentPoint.getX()-1);x<=currentPoint.getX()+1;x++){
                for (int y=(int)(currentPoint.getY()-1);y<=currentPoint.getY()+1;y++){
                    if ((image_in.getRGB(x,y)==pixel_color)){                            
                        if((!bound.contains(new MyPoint(x,y)))&(contour.contains(new MyPoint(x,y)))){
                            distance=new Dist(new MyPoint(x,y),Math.pow(currentPoint.getX()-x,2)+Math.pow(currentPoint.getY()-y,2));
                            found.add(distance);                            
                        }                        
                    } 
                }
            }
             
            Collections.sort(found,Dist.DistComparator);
            if (found.size()>0){
                currentPoint=found.get(0).getPoint();
                bound.add(currentPoint);
            }else{
                break;
            }            
            
            found.clear();           
        }     
                
        return bound;  
    }
    
    
    //---------------------------------------- contourTOcontour ----------------------------------------------------
    public static ArrayList<MyPoint> contourTOcontour(MyPoint pointZero,Arrayable array1, ArrayList<MyPoint> array2,BufferedImage image_in){
        //выбирает точки из внешней области непосредственно примыкающие к внутреннему контуру
        
        //pointZero - точка на внешней области непосредственно примыкающая к внутреннему контуру
        //array1 - внешняя область точек
        //array2 - это внуренний контур                       
        
        ArrayList<MyPoint> contour=array1.getArray();        
                
        return contourTOcontourF(pointZero, contour, array2, image_in);
    }
    
    public static ArrayList<MyPoint> contourTOcontour(MyPoint pointZero, ArrayList<MyPoint> array1, ArrayList<MyPoint> array2,BufferedImage image_in){
        //выбирает точки из внешней области непосредственно примыкающие к внутреннему контуру
        
        //pointZero - точка на внешней области непосредственно примыкающая к внутреннему контуру
        //array1 - внешняя область точек
        //array2 - внуренний контур       
                
        return contourTOcontourF(pointZero, array1, array2, image_in);  
    }
    
    public static ArrayList<MyPoint> contourTOcontour(MyPoint pointZero, Arrayable array1, Arrayable array,BufferedImage image_in){
        //выбирает точки из внешней области непосредственно примыкающие к внутреннему контуру
        
        //pointZero - точка на внешней области непосредственно примыкающая к внутреннему контуру
        //array1 - внешняя область точек
        //array2 - внуренний контур         
        
        ArrayList<MyPoint> contour=array1.getArray();  
        ArrayList<MyPoint> array2=array.getArray();        
                
        return contourTOcontourF(pointZero, contour, array2, image_in); 
    }
    
    public static ArrayList<MyPoint> contourTOcontour(MyPoint pointZero, ArrayList<MyPoint> array1, Arrayable array,BufferedImage image_in){
        //выбирает точки из внешней области непосредственно примыкающие к внутреннему контуру
        
        //pointZero - точка на внешней области непосредственно примыкающая к внутреннему контуру
        //array1 - внешняя область точек
        //array2 - внуренний контур 
        
        
        ArrayList<MyPoint> contour=array1; 
        ArrayList<MyPoint> array2=array.getArray();
        
        return contourTOcontourF(pointZero, contour, array2, image_in);
    }
    
    
    private static ArrayList<MyPoint> contourTOcontourF(MyPoint pointZero, ArrayList<MyPoint> array1, ArrayList<MyPoint> array2,BufferedImage image_in){
        //выбирает точки из внешней области непосредственно примыкающие к внутреннему контуру
        
        //pointZero - точка на внешней области непосредственно примыкающая к внутреннему контуру
        //array1 - внешняя область точек
        //array2 - внуренний контур 
        
        ArrayList<Dist> found= new ArrayList<>();  
        ArrayList<MyPoint> bound = new ArrayList<>();
        ArrayList<MyPoint> contour=array1;         
        MyPoint currentPoint=pointZero;
        Dist distance; 
        int pixel_color;         
               
        bound.add(currentPoint);
        while (true){                
            pixel_color=image_in.getRGB((int)currentPoint.getX(), (int)currentPoint.getY());
            
            for (int x=(int)(currentPoint.getX()-1);x<=currentPoint.getX()+1;x++){
                for (int y=(int)(currentPoint.getY()-1);y<=currentPoint.getY()+1;y++){
                    if ((image_in.getRGB(x,y)==pixel_color)){                            
                        if((!bound.contains(new MyPoint(x,y)))&(!contour.contains(new MyPoint(x,y)))&(array2.contains(new MyPoint(x,y)))){
                            distance=new Dist(new MyPoint(x,y),Math.pow(currentPoint.getX()-x,2)+Math.pow(currentPoint.getY()-y,2));
                            found.add(distance);                            
                        }                        
                    } 
                }
            }
             
            Collections.sort(found,Dist.DistComparator);
            if (found.size()>0){
                currentPoint=found.get(0).getPoint();
                bound.add(currentPoint);
            }else{
                break;
            }            
            
            found.clear();           
        }        
        return bound;  
        
    }
    
    //===================================================================================================
    
    private static class Dist{
        private final MyPoint point;
        private final double distance;
        
        public Dist(MyPoint point, double distance){
            this.point=point;
            this.distance=distance;            
        }

        /**
         * @return the point
         */
        public MyPoint getPoint() {
            return point;
        }

        /**
         * @return the distance
         */
        public double getDistance() {
            return distance;
        }

        public static Comparator<Dist> DistComparator = new Comparator<Dist>() {	
            @Override
            public int compare(Dist d1, Dist d2) {
                return (int) (d1.getDistance() - d2.getDistance());
            }        
        };
        
    
    }
    
}
